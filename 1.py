# Autor_ Pedro Vicente Merchan Infanate
# Email_ pedro.merchan@unl.edu.ec

# Read a string:
# s = input()
# Print a value:
# print(s)

s = input().split()
frecuencia = {}
for palabra in s :
  if palabra not in frecuencia:
    frecuencia[palabra] = 0
  print(frecuencia[palabra], end=' ')
  frecuencia[palabra] += 1
